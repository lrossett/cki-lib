"""A wrapper that compiles failure_regexes.yaml in an expected way."""
import os
import re

import anymarkup


class FailureRegexes:
    # pylint: disable=R0903
    """Compile failure_regexes.yaml in an expected way."""

    confpath = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            'failure_regexes.yaml')
    regexes = anymarkup.parse_file(confpath, 'yaml')

    for key, dct_val in regexes.items():
        regexes[key]['regexes'] = [
            re.compile(f'.*{v}.*', re.DOTALL | re.IGNORECASE) for v in
            dct_val['regexes']
        ]

    @classmethod
    def match_signature(cls, body):
        """Match signatures from FailureRegexes to specified text."""
        rxes = FailureRegexes.regexes
        for key in sorted(rxes.keys(), key=lambda k: rxes[k]['catchall']):
            symptom_item_dict = rxes[key]
            try:
                hints = symptom_item_dict['hints']
                if 'TOO_GENERIC' in hints:
                    # skip testing too generic regexes
                    continue
            except KeyError:
                # don't care if optional section 'hints' is missing
                hints = None

            for value in symptom_item_dict['regexes']:
                # Match against any regex -> return description
                if value.match(body):
                    # We have matched a know problem signature, return
                    # a human readable description.
                    return hints, symptom_item_dict['description']

        return None, None
