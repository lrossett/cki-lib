# Below are regexes that match indications of a problem in the pipeline.
# Syntax:
# <key>:
#  description: A human readable description of what happened
#  catchall: (true|false)
#  restartable: (true|false)
#  regexes:
#  - Python regex, passed to re.compile(f'.*{r}.*', re.DOTALL | re.IGNORECASE)
#  stage: <STAGE NAME>
#  hints:
#    - TOO_GENERIC
#    - NO_REPORT
#
#  key: must be uppercase and without spaces and special characters (except _).
#
#  catchall: set it to 'true' when the regexes contained are very generic and
#  should be matched against the data as a last resort (catchall) to cover
#  a bigger set of problems. Otherwise use false.
#
#  restartable: set it to 'true' when the issue caught by the regex is a
#  transient issue (e.g. networking issue) and re-running the script
#  (e.g. re-running the pipeline) should make the problem go away. This is
#  intended for automatic re-runs, so it should be used with great care and
#  only for issues that are really well-classified and cannot be mistaken with
#  a different kind of issue.
#
#  stage: use uppercase stage name (like TEST) to suggest during what stage
#  should this be used. This is merely a suggestion. Use ANY for any stage and
#  an empty string when the stage isn't known.
#
#  hints: an optional section; currently we recognize following hints:
#         NO_REPORT   - the report is not supposed to be sent
#         TOO_GENERIC - the regex captures a very generic condition, which
#                       does not include the probable cause of the issue
#
##########
# Example:
##########
# BOOT_FAILED:
#  description: A kernel failed to boot
#  catchall: true
#  regexes:
#  - 'state: retcode -> 3'
#  - \[state\].*?retcode = 3
#  stage: TEST
BEAKER_NO_TESTS:
  description: Likely there's no test set specified for this
  catchall: false
  regexes:
    - 'No RecipeSets! You can not have a Job with no recipeSets'
  stage: TEST
  restartable: false
BEAKER_NETWORK:
  description: Problem connecting to Beaker
  catchall: false
  regexes:
  - WARNING XML-RPC connection to beaker
  stage: TEST
  restartable: true
BEAKER_TESTS_CANCELLED:
  description: Generic infrastructure issue in Beaker
  catchall: false
  regexes:
  - 'All test sets aborted or were cancelled!'
  stage: TEST
  restartable: false
BUILD_ERROR_MAKE:
  description: Build failed with make error
  catchall: true
  regexes:
  - 'make[^\n]+Error 2'
  stage: BUILD
  restartable: false
KPET_ERROR:
  description: Test metadata problem (kpet)
  catchall: false
  regexes:
  - 'Error: While executing command "run"'
  - 'Exception: No test sets matched specified'
  stage: SETUP
  restartable: false
KPET_DISTRO_ISSUE:
  description: Distro problem, check kpet-db and Beaker
  catchall: false
  regexes:
  - 'No distro tree matches Recipe'
  stage: TEST
  restartable: false
APPLY_PATCH:
  description: Failed to apply a patch
  catchall: false
  regexes:
  - 'Trace: If you prefer to skip this patch'
  - ERROR\s+Failed to apply patch
  - 'Trace: Patch failed at'
  - ' patch does not apply'
  stage: MERGE
  hints:
    - TOO_GENERIC
  restartable: false
ARTIFACTS_MISSING:
  description: Gitlab artifacts missing (expired?)
  catchall: false
  regexes:
  - Archive:\s+artifacts.zip.*?End-of-central-directory
  stage: ANY
  restartable: false
BAD_YAML:
  description: pipeline-definition problem
  catchall: true
  regexes:
  - Pipeline.*had 0 failed builds
  stage: ANY
  restartable: false
BOOT_FAILED:
  description: A kernel failed to boot
  catchall: true
  regexes:
  - 'state: retcode -> 3'
  - \[state\].*?retcode = 3
  stage: TEST
  hints:
    - TOO_GENERIC
  restartable: false
BUILD_FAIL:
  description: A kernel failed to build
  catchall: false
  regexes:
  - 'Stage: build.*?INSTALL.*?.ko.*?terminated with exit code 137'
  - 'Stage: build.*?Your architecture did not define any architecture-dependent files.*?terminated
    with exit code 137'
  stage: BUILD
  hints:
    - TOO_GENERIC
  restartable: false
CACHE:
  description: Cache problem
  catchall: false
  regexes:
  - Failed to extract cache
  - 'fatal: repository .*?cache.*? does not exist'
  stage: ANY
  restartable: false
CODE_BREAKAGE:
  description: Possibly an issue in CKI code
  catchall: true
  regexes:
  - 'Traceback \(most recent call last\)'
  - 'ModuleNotFoundError: No module named'
  - 'jinja2.exceptions.TemplateNotFound:'
  - 'rhcheckpatch.py: error:'
  - object has no attribute
  - local variable .*? referenced before assignment
  - 'Trace:.*KeyError:'
  - 'curl: no URL specified'
  - 'could not create work tree dir.*?: Permission denied'
  - 'TypeError: .*? object is not subscriptable'
  - You must give at least one requirement to install
  - ': command not found'
  - 'ERROR: .*? site-packages are not visible in this virtualenv'
  - 'UnicodeEncodeError: .*? codec'
  - 'Trace: Section not found: state'
  stage: ANY
  restartable: false
CONTAINER_REPO:
  description: Possible container registry problem
  catchall: false
  regexes:
  - 'Job failed: image pull failed'
  stage: ANY
  restartable: true
DEPENDS_NOT_SUPPORTED:
  description: Patch has a Patch Dependency, which is not supported for now
  catchall: false
  regexes:
  - Patch Dependency is not supported for now
  - from the \'Depends On\' field in
  stage: LINT
  restartable: false
DEPLOY_ERR:
  description: Deploy stage failed
  catchall: true
  regexes:
  - 'Stage: deploy\nName: deploy'
  stage: DEPLOY
  restartable: false
GIT_REPO:
  description: A problem with git repo
  catchall: false
  regexes:
  - 'subprocess.CalledProcessError: Command.*git'
  - 'remove_remote_git_branch.*?deleted.*?ERROR: Job failed: exit code'
  restartable: false
GIT_REPO_FORCE_PUSH:
  description: A problem with git repo. Possibly a force-push by an evil user.
  catchall: false
  regexes:
  - "server doesn't allow unadvertized objects"
  - 'fatal: reference is not a tree:'
  stage: MERGE
  hints:
    - NO_REPORT
  restartable: false
HTTP_500:
  description: HTTP server responded with code 500
  catchall: false
  regexes:
  - Internal Server Error
  stage: ANY
  restartable: true
BEAKER_OUTAGE:
  description: Cannot submit a job, possibly a Beaker outage
  catchall: false
  regexes:
  - 'Submitted: \[\].*?ERROR.*?Unable to submit the job!'
  stage: TEST
  restartable: true
INFRA_FAIL:
  description: Generic infrastructure failure
  catchall: true
  regexes:
  - 'state: retcode -> 2'
  - \[state\].*?retcode = 2
  stage: TEST
  hints:
    - TOO_GENERIC
  restartable: false
KOJI_BUILD_MISSING:
  description: Koji build is missing (expired?)
  catchall: false
  regexes:
  - 'Stage: createrepo\nName: createrepo ppc64le.*?Giving up after'
  stage: CREATEREPO
  restartable: false
LINUX_PACKAGE_MISSING:
  description: Package missing
  catchall: false
  regexes:
  - unable to execute \'gcc\'
  - 'fatal error: Python\.h: No such file or directory'
  - This command has to be run under the root user
  stage: ANY
  restartable: false
NETWORK:
  description: A networking issue
  catchall: false
  regexes:
  - 'DistutilsError: Download error for'
  - '503 Server Error: Service Unavailable'
  - '502 Server Error: Proxy Error'
  - Max retries exceeded with url
  - TLS handshake timeout
  - connection reset by peer
  - the remote end hung up unexpectedly
  - request timed out
  - ChunkedEncodingError.*IncompleteRead
  - 'fatal: unable to access ''https'
  - 'RuntimeError: cannot get query result'
  - couldn\'t execute POST against
  - ConnectionError.*?Connection aborted
  - 'ReadTimeoutError: HTTPSConnectionPool'
  - The read operation timed out
  - 'requests.exceptions.ReadTimeout'
  - 'gitlab.exceptions.GitlabListError: 502: GitLab is not responding'
  - 'TimeoutError: [Errno 110] Connection timed out'
  - 'Name or service not known'
  stage: ANY
  restartable: true
OUTAGE:
  description: Ongoing service outage
  catchall: false
  regexes:
  - Cannot find KDC for requested realm
  stage: ANY
  restartable: true
PERMS:
  description: Permission problem occurred
  catchall: false
  regexes:
  - 'remote: Your account has been blocked'
  - 'pods.*is forbidden: User.*?cannot create pods'
  - Existing credentials are invalid, please enter valid username
  - 'Job failed \(system failure\): Unauthorized'
  - 'unauthorized: incorrect username or password'
  stage: ANY
  restartable: false
PIPELINE_TRIGGERED_TWICE:
  description: Pipeline triggered twice
  catchall: false
  regexes:
  - 'line [0-9]+: nvr: unbound variable'
  stage: ANY
  restartable: false
PIP_PACKAGE_MISSING:
  description: pip package is missing
  catchall: false
  regexes:
  - Please install Cython or download a release package of
  stage: ANY
  restartable: false
QUOTA:
  description: quota exceeded
  catchall: false
  regexes:
  - 'is forbidden: exceeded quota'
  stage: ANY
  restartable: false
RHCHECKPATCH_FAIL:
  description: Patch doesn't conform to RHEL rules
  catchall: false
  regexes:
  - 'patch\(es\) checked, ERRORS: [0-9]+, WARNINGS: [0-9]+'
  stage: LINT
  restartable: false
RHCHECKPATCH_SEC_SENSITIVE:
  description: Patch(es) posted on the wrong mailing list
  catchall: false
  regexes:
  - marked security-sensitive, send patch\(es\) to
  stage: LINT
  restartable: false
RH_SRPM:
  description: build failed (srpm subtarget problem)
  catchall: false
  regexes:
  - make.*?rh-srpm.*?Error 2
  stage: BUILD
  restartable: false
RUNNER_ERR:
  description: Gitlab runner problem
  catchall: false
  regexes:
  - 'ERROR: Job failed \(system failure\):'
  stage: ANY
  restartable: true
TEST_FAIL:
  description: Testing failed
  catchall: true
  regexes:
  - 'state: retcode -> 1'
  - \[state\].*?retcode = 1
  stage: TEST
  hints:
    - TOO_GENERIC
  restartable: false
TIMEOUT:
  description: Service timeout
  catchall: false
  regexes:
  - 'Job failed: execution took longer than'
  - 'Job failed \(system failure\): timeout'
  stage: ANY
  restartable: false
TOKEN:
  description: Token problem
  catchall: false
  regexes:
  - No API token found for service account "default"
  stage: ANY
  restartable: false
TRIGGER_ERR:
  description: Failed to trigger a pipeline
  catchall: true
  regexes:
  - 'Stage: run\nName: run cki github'
  - 'Stage: run\nName: run patch trigger'
  - 'Stage: run\nName: run pw1 trigger'
  - 'Stage: run\nName: run stable queue'
  stage: ANY
  restartable: false
UPSHIFT:
  description: Upshift issue (ongoing outage?)
  catchall: false
  regexes:
  - 'getsockopt: connection refused'
  - Job failed.*?unexpected EOF
  - to return a response in the time allotted
  - namespaces/ark/pods.*onnection reset by peer
  stage: ANY
  restartable: true
