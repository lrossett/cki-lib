"""YAML utils."""
import yaml


# pylint: disable=too-many-ancestors
class BlockDumper(yaml.SafeDumper):
    """Block-style literals for strings with newlines.

    This can be used via
        from cki_lib.yaml import BlockDumper
        yaml.dump(what, Dumper=BlockDumper, **kwargs)
    """

    def represent_str(self, data):
        """Format strings with newlines as block-style literals."""
        if '\n' not in data:
            return super().represent_str(data)
        return self.represent_scalar(u'tag:yaml.org,2002:str', data, style='|')


yaml.add_representer(str, BlockDumper.represent_str, Dumper=BlockDumper)
