"""Fake Gitlab classes."""
# pylint: disable=too-few-public-methods
from unittest import mock


class FakeGitLabCommits():
    """Fake commit."""

    def __init__(self):
        """Initialize."""
        self._commits = []

    def create(self, data):
        """Create a commit."""
        self._commits.append(data)

    def __getitem__(self, index):
        """Get a commit."""
        return self._commits[index]


class FakePipelineJob():
    """Fake pipeline job."""

    def __init__(self, job_id):
        """Initialize."""
        self.attributes = {'id': job_id}


class FakePipelineJobs():
    """Fake pipeline jobs."""

    def __init__(self):
        """Initialize."""
        self._jobs = []
        self.add_job(1)

    def add_job(self, job_id):
        """Add a job."""
        self._jobs.append(FakePipelineJob(job_id))

    def list(self, page=None, per_page=None):
        """List all jobs."""
        return self._jobs


class FakePipelineVariables():
    """Fake pipeline variables."""

    def __init__(self, variables):
        """Initialize."""
        self._variables = variables

    @staticmethod
    def _as_key_value(variables):
        return [mock.MagicMock(key=key, value=value)
                for key, value in variables.items()]

    def list(self, **kwargs):
        """List all variables."""
        kwargs.pop('as_list', None)
        if not kwargs:
            return self._as_key_value(self._variables)

        raise NotImplementedError


class FakePipeline():
    """Fake pipeline."""

    pipeline_counter = 0

    def __init__(self, branch, token, variables, status):
        """Initialize."""
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = FakePipelineVariables(variables)
        self.id = FakePipeline.pipeline_counter
        self.web_url = 'dummy-url'
        FakePipeline.pipeline_counter += 1


class FakeProjectPipelines():
    """Fake project pipelines."""

    def __init__(self):
        """Initialize."""
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        """Add a new pipeline."""
        pipeline = FakePipeline(branch, token, variables, status)
        self._pipelines.append(pipeline)
        return pipeline

    def __getitem__(self, index):
        """Get a pipeline."""
        return self._pipelines[index]

    def list(self, **kwargs):
        """List all pipelines."""
        kwargs.pop('as_list', None)
        if not kwargs:
            return self._pipelines

        to_return = []
        for pipeline in self._pipelines:
            if pipeline.branch == kwargs.get('ref'):
                to_return.append(pipeline)
            elif pipeline.attributes['status'] == kwargs.get('status'):
                to_return.append(pipeline)

        return to_return

    def create(self, payload):
        """Trigger a pipeline."""
        return self.add_new_pipeline(payload['ref'],
                                     None,
                                     {v['key']: v['value'] for v in payload['variables']},
                                     'pending')


class FakeProjectBranches():
    """Fake project branches."""

    def __init__(self):
        """Initialize."""
        self._branches = {}

    def add_new_branch(self, branch):
        """Add a new branch."""
        self._branches[branch] = {}

    def get(self, branch):
        """Add a branch."""
        return self._branches[branch]


class FakeGitLabProject():
    """Fake project."""

    def __init__(self):
        """Initialize."""
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()
        self.branches = FakeProjectBranches()
        self.manager = mock.MagicMock()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        """Trigger a pipeline."""
        return self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    """Fake gitlab."""

    def __init__(self):
        """Initialize."""
        self.projects = {}

    def add_project(self, project_name):
        """Add a project."""
        self.projects[project_name] = FakeGitLabProject()
        return self.projects[project_name]
