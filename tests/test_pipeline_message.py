"""PipelineMessage interaction tests."""

import mailbox
import unittest

from cki_lib.misc import tempfile_from_string
from cki_lib.pipeline_message import PipelineMsgBase


def craft_mbox(path, headers, body):
    """Create mbox at path with headers and body."""
    testmbox = mailbox.mbox(path)
    testmbox.lock()
    try:
        msg = mailbox.mboxMessage()
        msg.set_unixfrom('author Sat Feb  1 02:00:00 2009')
        for hkey, hvalue in headers.items():
            msg[hkey] = hvalue
        msg.set_payload(body)
        testmbox.add(msg)
        testmbox.flush()
    finally:
        testmbox.unlock()

    return testmbox


class TestPipelineMessage(unittest.TestCase):
    """Test PipelineMessage class."""

    def test_init(self):
        """Test PipelineMessageBase() behavior."""
        headers = {'From': 'from@from.com', 'To': 'to@to.com',
                   'Message-ID': '<1234-4@1234.com>',
                   'Subject': 'Some\t\tsubject'}

        with tempfile_from_string(b'') as fpath:
            testmbox = craft_mbox(fpath, headers, 'Test body')
            pipemsg = PipelineMsgBase(testmbox[0])
            self.assertIsNone(pipemsg.pipelineid)

        headers['X-Gitlab-Pipeline-ID'] = '0'

        with tempfile_from_string(b'') as fpath:
            testmbox = craft_mbox(fpath, headers, 'Test body')
            pipemsg = PipelineMsgBase(testmbox[0])
            self.assertEqual(pipemsg.pipelineid, 0)
            self.assertEqual(pipemsg.subject, 'Some subject')

    def test_subject_missing(self):
        """Ensure missing subject gets set to None."""
        headers = {'From': 'from@from.com', 'To': 'to@to.com',
                   'Message-ID': '<1234-4@1234.com>'}

        with tempfile_from_string(b'') as fpath:
            testmbox = craft_mbox(fpath, headers, 'Test body')
            pipemsg = PipelineMsgBase(testmbox[0])
            self.assertIsNone(pipemsg.subject)

    def test_from_file(self):
        """Ensure from_file works."""
        with tempfile_from_string(b'') as fpath:
            craft_mbox(fpath, {}, 'Test body')
            pipemsg = PipelineMsgBase.from_file(fpath)[0]
            self.assertEqual(pipemsg.body, 'Test body\n')
