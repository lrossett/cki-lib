"""Test timer module."""
import time
import unittest
from unittest import mock

from cki_lib import timer


class TestScheduledTask(unittest.TestCase):
    """Test ScheduledTask."""

    def test_start(self):
        """Test that callback is called."""
        callback = mock.Mock()
        task = timer.ScheduledTask(0, callback)
        task.start()
        self.assertEqual(1, callback.call_count)

    def test_start_many_times(self):
        """
        Test calling start multiple times.

        If start() calls happen before the timer expired the callback
        should only be called once.
        """
        callback = mock.Mock()
        task = timer.ScheduledTask(0.1, callback)
        # Call task.start multiple times.
        for _ in range(3):
            task.start()
        time.sleep(0.2)  # The interval the timer is not exact.
        self.assertEqual(1, callback.call_count)

    def test_start_args(self):
        """Test that callback is called with all the args."""
        callback = mock.Mock()
        task = timer.ScheduledTask(0, callback, 1, 2, a=3, b=4)
        task.start()
        self.assertEqual(1, callback.call_count)
        callback.assert_called_with(1, 2, a=3, b=4)

    def test_cancel(self):
        """Test that cancels execution."""
        callback = mock.Mock()
        task = timer.ScheduledTask(0.1, callback)
        task.start()
        task.cancel()
        time.sleep(0.2)  # The interval the timer is not exact.
        self.assertEqual(0, callback.call_count)
